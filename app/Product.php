<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //many Products "belongs to many" Orders (MANY-to-MANY) ----revised below
    
	//a PRODUCT belongsToMany orders
    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot("quantity")->withTimeStamps();
    }

    //a PRODUCT belongsTo a single category
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

}
