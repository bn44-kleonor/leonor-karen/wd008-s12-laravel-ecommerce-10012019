<div id="alert_container">
	@if(Session::has("success"))
		<div class="alert alert-success" id="alert_success">
			{{ Session::get('success') }}
		</div>
	@elseif(Session::has("error"))
		<div class="alert alert-danger" id="alert_error">
			{{ Session::get('error') }}
		</div>
	@endif	
</div>
