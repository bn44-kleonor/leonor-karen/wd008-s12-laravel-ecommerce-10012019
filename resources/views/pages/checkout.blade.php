@extends('layouts.app')
@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="display-4">
           {{ $title }}
        </div>
    </div>
</div>
<form action="/confirmation/{{ $user->id }}" method="POST" id="">
    @csrf
    <!-- kapag POST ang method, we don't need to use "method_field" -->
    <div class="row mb-4">
        <!-- ORDER SUMMARY -->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Order</h5>
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" width="20%">Name</th>
                                <th scope="col" width="20%">Price</th>
                                <th scope="col" width="20%">Quantity</th>
                                <th scope="col" width="20%">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody id="cart-body">
                        @foreach($cart_products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>&#36; {{ $product->price }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>&#36; {{ $product->subtotal }}</td>
                            </tr>
                          
                            <tr>
                                <td colspan="3" class="text-right">
                                    <h4>Total</h4>
                                </td>
                                <td class="text-left">
                                    <h4>&#36; {{ $total }} </h4>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <!-- USER DETAILS -->
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Delivery Address</h5>

                    <!-- FIRST NAME -->
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="first_name" value="{{ $user->first_name}}" placeholder="Enter first name">
                    </div>

                    <!-- LAST NAME -->
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="last_name" value="{{ $user->last_name}}" placeholder="Enter last name">
                    </div>

                    <!-- ADDRESS -->
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="address" id="address" aria-describedby="address" value="{{ $user->address}}" placeholder="Address">
                    </div>

                    <!-- CONTACT NUMBER -->
                    <div class="form-group">
                        <label for="contact_number">Contact Number</label>
                        <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" class="form-control" name="contact_number" id="contact_number" aria-describedby="contact_number" value="{{ $user->contact_number}}" placeholder="Format: 123-456-7890">
                    </div>

                    <!-- TOTAL -->
                    <input type="hidden" id="total" name="total" value="{{ $total }}">

                    <!-- USERID -->
                    <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="d-flex">

                <div class="mr-auto">
                    <a href="{{ URL::previous() }}" class='btn btn-secondary'>Back</a>
                </div>

                <!-- PAYMENT BUTTON -->
                <div>
                    <button class='btn btn-primary'>Pay Via COD</button>
                </div>

                <!-- PAYPAL BUTTON -->
                <div id="paypal-button-container"></div>
            </div>
        </div>
    </div>
</form>
@endsection

